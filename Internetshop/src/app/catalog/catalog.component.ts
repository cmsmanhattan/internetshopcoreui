import{Component,OnInit}from'@angular/core';
import{ProductsService}from'../products.service';

import{HttpClient,HttpHeaders}from'@angular/common/http';



interface Admin {
	  post_manager: string;
	  post_manager_img: string;
	  post_manager_text: string;
	}

interface XslStyle {
	xsl_url: string;
	xsl_url_text: string;
}

interface Currency {
	  code: string;
	  description: string;
	}

interface Product {
	product_id: string;
	row_id: string;
	name: string;
	type_id: string;
	icon: string;
	image: string;
	user_id: string;
	policy_url: string;
	description: string;
	amount:string;
	currency: Currency;
	version:string;
	creteria1:string;
	creteria2:string;
	creteria3:string;
	creteria4:string;
	creteria5:string;
	creteria6:string;
	creteria7:string;
	creteria8:string;
	creteria9:string;
	creteria10:string;
	color:string;

	}

interface ProductBlog {

product_id:string;
row_id:string;
file_exist:string;
name:string;
parent_title:string;
product_parent_id:string;
icon:string;
image:string;
user_id:string;
author:string;
company:string;
policy_url:string;
description:string;
amount:string;
currency:Currency;
version:string;
statistic:string;
cdate:string;

}

interface News {
	  page_url: string;
	  product_id: string;
	  name: string;
	  file_exist: string;
	  icon: string;
	  image: string;
	  big_image_type: string;
	  icon_type: string;
	  user_id: string;
	  policy_url: string;
	  description: string;
	  amount: string;
	  currency: Currency
	  statistic: string;
	  version: string;
	  cdate: string;
	  creator_info_user_id: string;
	}

interface Bottom {

	  product_id: string;
	  row_id: string;
	  file_exist: string;
	  name: string;
	  icon: string;
	  image: string;
	  big_image_type: string;
	  icon_type: string;
	  user_id: string;
	  policy_url: string;
	  product_url: string;
	  description: string;
	  amount: string
	  currency: Currency;
	  version: string;

	}

interface CurrenciesItem { 
	selected: string;
	item: string;
	code: string;
	url: string;
}

interface CatalogItem { 
	
	selected: string;
	item: string;
	code: string;
	url: string;
    subcatalogItem: string;
    subitem: string;
    subcode: string;
    suburl: string;
}

interface MenuItem {
	selected:string;item:string;code:string;url:string;
}

interface Creteria1Item {
	selected:string;item:string;code:string;url:string;
}

interface DayFromItem {
	selected:string;item:string;code:string;url:string;
}

interface MountFromItem {
	selected:string;item:string;code:string;url:string;
}

interface YearFromItem {
	selected:string;item:string;code:string;url:string;
}

interface DayToItem {
	selected:string;item:string;code:string;url:string;
}

interface MountToItem {
	selected:string;item:string;code:string;url:string;
}

interface YearToItem {
	selected:string;item:string;code:string;url:string;
}

interface ParentItem {
	selected:string;item:string;code:string;url:string;
}



interface PageModel {

	version:string;name:string;title:string;reklama:string;subject_site:string;site_name:string;host:string;message:string;login:string;passwdord:string;balans:string;search_value:string;search_query:string;fromcost:string;tocost:string;to_navigator:string;to_navigator_location:string;to_account_history:string;to_pay:string;to_order:string;to_order_hist:string;to_login:string;to_registration:string;owner_user_id:string;role_id:string;user_site_id:string;site_id:string;path:string;dialog:string;is_advanced_search_open:string;is_forum_open:string;internet:string;admin:Admin;xslStyle:XslStyle;

	//product_list:Product[];
	product_list:any[];
	
	 // <coproductlist1> </coproductlist1>
	 
	 // <coproductlist2> </coproductlist2>

	coproductlist1:any[];

	coproductlist2:any[];

	product_blog_list:any[];

	newslist:any[];

    bottomlist:any[];

	empty_page_co1:string;

	empty_page_co2:string;

	empty_page:string;

	quantity_products:string;

	offset:string;next:string;prev:string;

	criteria1_label:string;criteria2_label:string;criteria3_label:string;criteria4_label:string;criteria5_label:string;criteria6_label:string;criteria7_label:string;criteria8_label:string;criteria9_label:string;criteria10_label:string;

	currencies:CurrenciesItem[];
    catalog:CatalogItem[];
    menu:any[];

	creteria1:Creteria1Item[];creteria2:Creteria1Item[];creteria3:Creteria1Item[];creteria4:Creteria1Item[];creteria5:Creteria1Item[];creteria6:Creteria1Item[];creteria7:Creteria1Item[];creteria8:Creteria1Item[];creteria9:Creteria1Item[];creteria10:Creteria1Item[];

	dayFrom:DayFromItem[];mountFrom:MountFromItem[];yearFrom:YearFromItem[];

	dayTo:DayToItem[];mountTo:MountToItem[];yearTo:YearToItem[];

	parent:ParentItem[];

}


@Component
({
	selector:'app-catalog',
	templateUrl:'./catalog.component.html',
	styleUrls:['./catalog.component.css']
})
export class CatalogComponent implements OnInit {


public productList:Product[] = [] ;

public coproductList1:Product[] = [] ;

public coproductList2:Product[] = [] ;

public productBlogList:ProductBlog[] =  [] ;

public  newsList:News[] = [] ;

public  bottomList:Bottom[] =  [] ;

public  menuList:MenuItem[] =  [] ;


  constructor(  private productsService : ProductsService , private _http: HttpClient ) {}
   
  

	ngOnInit() {
		let data =  this.productsService.getProducts().subscribe((data) => {  
		
		this.productsService.parseXML(data)  
          .then((data) => {  
			  debugger
			 this.setProductList(<PageModel>data) ;
			  this.setCoproductList1(<PageModel>data) ;
			  this.setCoproductList2( <PageModel>data) ;
			  this.setProductBlogList(<PageModel>data) ;
			  this.setNewsList(<PageModel>data) ;
			  this.setBottomList(<PageModel>data) ;
			  this.setMenuList(<PageModel>data) ;
			  
		  });

	  });  
  }

	private setProductList( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.product_list)
			{
				
			  if( item.product instanceof Array == false) return ;
			  for (let product of item.product)
				{
					product = product as Product ;
					this.productList.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
				
			}
	  }
	
	
	  
	private setCoproductList1( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.coproductlist1)
			{
				
			  if( item.product instanceof Array == false) return ;
			  
			  	for (let product of item.product)
				{
					product = product as Product ;
					this.coproductList1.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
				
			}
	  }
	  
	
	
	private setCoproductList2( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.coproductlist2)
			{
				
			  if( item.product instanceof Array == false) return ;
			  for (let product of item.product)
				{
					product = product as Product ;
					this.coproductList2.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
				
			}
	  }
	  
	
	
	private setProductBlogList( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.product_blog_list)
			{
				
			  if( item.product_blog instanceof Array == false) return ;
			  for (let product of item.product_blog)
				{
					product = product as Product ;
					this.productBlogList.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
				
			}
	  }
	  
	//newslist
	private setNewsList( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.newslist)
			{
				
			  if( item.news instanceof Array == false) return ;
			  for (let product of item.news)
				{
					product = product as News ;
					this.newsList.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
				
			}
	  }
	  
	
	private setBottomList( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.bottomlist)
			{
				
			  if( item.bottom instanceof Array == false) return ;
			  for (let product of item.bottom)
				{
					product = product as Bottom ;
					this.bottomList.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
				
			}
	  }
	  
	//public  menuList:MenuItem[] =  [] ;
	//item.menu-item 
	private setMenuList( xmlDoc : PageModel )
	  {
		  for (let item of xmlDoc.menu)
			{
			  
			 
			  //if( item.menu instanceof Array == false) return ;
			  for (let product of item.menu-item )
				{
					product = product as MenuItem ;
					this.menuList.push(product) ;
					console.log("data name : " + product.name  ) ;
				}
			 
				
			}
	  }


}
