import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import xml2js from 'xml2js';
import { HttpClient, HttpHeaders } from '@angular/common/http';


interface Admin {

  post_manager: string;
  post_manager_img: string;
  post_manager_text: string;

}

interface Currency {
  code: string;
  description: string;
}

interface Currencies {
  selected: string;
  item: string;
  code: string;
  url: string;
}

interface Product {

  page_url: string;
  product_id: string;
  name: string;
  file_exist: string;
  icon: string;
  image: string;
  image_type: string;
  product_url: string;
  back_url: string;
  description: string;
  amount: string;
  currency: Currency
  statistic: string;
  cdate: string;
  creator_info_user_id: string;

}


interface News {
  page_url: string;
  product_id: string;
  name: string;
  file_exist: string;
  icon: string;
  image: string;
  big_image_type: string;
  icon_type: string;
  user_id: string;
  policy_url: string;
  description: string;
  amount: string;
  currency: Currency
  statistic: string;
  version: string;
  cdate: string;
  creator_info_user_id: string;
}


interface Bottom {

  product_id: string;
  row_id: string;
  file_exist: string;
  name: string;
  icon: string;
  image: string;
  big_image_type: string;
  icon_type: string;
  user_id: string;
  policy_url: string;
  product_url: string;
  description: string;
  amount: string
  currency: Currency;
  version: string;

}


interface Rating1 {
  show_star_1: string;
  show_star_2: string;
  show_star_3: string;
  show_star_4: string;
  show_star_5: string;
  show_star_6: string;
  show_star_7: string;
  show_star_8: string;
  show_star_9: string;
  show_star_10: string;
}


interface CatalogItem {
  selected: string;
  item: string;
  code: string;
  url: string;
  subcatalogItem: string;
  subitem: string;
  subcode: string;
  suburl: string;
}


interface MenuItem {
  selected: string;
  item: string;
  code: string;
  url: string;
}

interface ParentItem {
  selected: string;
  item: string;
  code: string;
  url: string;
}




interface PageModel {

  document: string;
  version: string;
  name: string;
  title: string
  subject_site: string;

  site_name: string;
  host: string;
  domain: string;
  admin: Admin;

  role_id: string;
  user_site_id: string;
  internet: string;
  login: string;
  message: string;
  shoping_url: string;
  balans: string;
  to_account_history: string;
  to_login: string;
  to_registration: string;
  to_order: string;
  to_order_hist: string;
  to_pay: string;
  owner_user_id: string;
  site_id: string;
  show_blog: string;
  show_rating1: string;
  show_rating2: string;
  show_rating3: string;

  product: Product;

  rating1: Rating1;

  currencies: Currencies;

  /**
  <extpolicy_productlist1>
  </extpolicy_productlist1>
  
  <extpolicy_productlist2>
  </extpolicy_productlist2>
  
  
  <extpolicy_file_list>
  </extpolicy_file_list>
  
  <extpolicy_list_tabs>
  </extpolicy_list_tabs>
  
  
  <product_blog_list>
  </product_blog_list>
  */

  newslist: News[];
  bottomlist: Bottom[];

  empty_page_ext1: string;
  empty_page_ext2: string;

  catalog: CatalogItem[];

  parent: ParentItem[];

  menu: MenuItem[];

}

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  public doc: PageModel;
  public genericDoc: any;

  constructor(private productsService: ProductsService, private _http: HttpClient) { }

  ngOnInit() {

    let data = this.productsService.getProducts().subscribe((data) => {
      this.parseXMLExample(data)
        .then((data) => {
          this.genericDoc = data;
          console.log("data: " + data);

        });
    });


    // console.log("data: " + data. ) ;

  }


  private parseXMLExample(data) {
    return new Promise(resolve => {
      var k: string | number,
        arr = [],
        parser = new xml2js.Parser(
          {
            trim: true,
            explicitArray: true
          });
      parser.parseString(data, function (err, result) {

        /** 
         
        var obj = result.Employee;  
        for (k in obj.emp) {  
          var item = obj.emp[k];  
          arr.push({  
            id: item.id[0],  
            name: item.name[0],  
            gender: item.gender[0],  
            mobile: item.mobile[0]  
          });  
        }  
        */
        resolve(arr);
      });
    });
  }


  loadXMLExample() {  
    this._http.get('/assets/users.xml',  
      {  
        headers: new HttpHeaders()  
          .set('Content-Type', 'text/xml')  
          .append('Access-Control-Allow-Methods', 'GET')  
          .append('Access-Control-Allow-Origin', '*')  
          .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method"),  
        responseType: 'text'  
      })  
      .subscribe((data) => {  
        this.parseXMLExample(data)  
          .then((data) => {  
            this.genericDoc = data;  
          });  
      });  
  } 

}
