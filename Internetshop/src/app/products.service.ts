import { Injectable } from '@angular/core';
import { HttpHeaders,HttpClient,HttpParams } from '@angular/common/http'
import { Observable }  from 'rxjs';
import { ActivatedRoute } from '@angular/router'
import { map  , switchMap} from 'rxjs/operators'
//import { bindNodeCallback } from 'rxjs/observable/bindNodeCallback';
import xml2js from'xml2js';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {


//let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded' });

//let options = new RequestOptions({ headers: headers, method: RequestMethod.Post}); 

  
  private httpOptionsGet = {
    headers: new HttpHeaders({
      'Content-Type': 'text/xml',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method',
       responseType: 'text'
    })


  } ;

  private httpOptionsExt = {
    headers: new HttpHeaders({
      'Content-Type': 'appication/xml',
      'sessionID':'sesstionId',
      'userName' : 'user'
    })
  } ;

    private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'appication/xml'
    })
  } ;

  constructor(private http: HttpClient  ) { }

  public getProducts1(): Observable<any> 
  {
     return this.http.get(('/API/Productlist.jsp'),this.httpOptions);

  }

  public getProducts(): Observable<any> 
  {
     return this.http.get('/API/Productlist.jsp',  
      {  
        headers: new HttpHeaders()  
          .set('Content-Type', 'text/xml')  
          .append('Access-Control-Allow-Methods', 'GET')  
          .append('Access-Control-Allow-Origin', '*')  
          .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method"),  
        responseType: 'text'  
      }) 
  }

  public saveProducts(jsonObj: any ): Observable<any> 
  {
   return this.http.post(('/API/Productlist.jsp'),JSON.stringify(jsonObj),this.httpOptions);
  }

/*

public getProducts1(): Observable<any> 
  {
  vket ar headers = new Headers();

h eaders.append('Accept', 'application/xml');

return this.http.get('https://angular2.apispark.net/v1/companies/', {
  headers: headers
}).map(res => JSON.parse(xml2json(res.text(),'  ')));

 }
*/

/** 
public list() {
  return this.http
    .get(('/API/Productlist.jsp'),this.httpOptions)
    //.pipe(switchMap(res => Observable.bindNodeCallback(xml2js.parseString)(res)))
    .pipe(switchMap(res => Observable.bind(xml2js.parseString)(res)))
    .subscribe(console.log);
}
*/
  
  
 public parseXML(data : any) {  
	    return new Promise(resolve => {  

	        var k: string | number,  
	        parser = new xml2js.Parser(  
	          {  
	            trim: true,  
	            explicitArray: true  
	          });  
		  
			 parser.parseString(data, function (err, result) {  

				

	       	 resolve(result.document);  
	      });  
	    });  
	  }  

}
