import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CatalogComponent } from './catalog/catalog.component';
import { PageComponent } from './page/page.component';
import { RegistrationComponent } from './registration/registration.component';
import { AppRoutingModule } from './app-routing.module';
import { OrderComponent } from './order/order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { AccountHistoryComponent } from './account-history/account-history.component';
import { AccountHistoryDetailComponent } from './account-history-detail/account-history-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    PageComponent,
    RegistrationComponent,
    OrderComponent,
    OrderHistoryComponent,
    AccountHistoryComponent,
    AccountHistoryDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
