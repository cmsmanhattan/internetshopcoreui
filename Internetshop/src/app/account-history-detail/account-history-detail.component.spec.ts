import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountHistoryDetailComponent } from './account-history-detail.component';

describe('AccountHistoryDetailComponent', () => {
  let component: AccountHistoryDetailComponent;
  let fixture: ComponentFixture<AccountHistoryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountHistoryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
