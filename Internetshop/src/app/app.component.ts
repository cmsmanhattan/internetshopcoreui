import { Component , OnInit } from '@angular/core';
import {ProductsService } from './products.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Internetshop';
  productList : any ;

  ngOnInit(): void {
    this.productList = this.products.getProducts() ;
  }

 
  constructor( private products : ProductsService  ){

    console.log("constructor AppComponent started with  ProductsService")
  }

}
