import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'price', 'category', 'action'];
  dataSource: any;

  constructor() { }

  ngOnInit(): void {
    this.dataSource = [
      { position: 1, name: 'Product 1', price: 600, category: 'Category 1' },
      { position: 2, name: 'Product 2', price: 500, category: 'Category 2' },
      { position: 3, name: 'Product 3', price: 400, category: 'Category 3' },
      { position: 4, name: 'Product 4', price: 922, category: 'Category 4' },
      { position: 5, name: 'Product 5', price: 811, category: 'Category 5' },
      { position: 6, name: 'Product 6', price: 2107, category: 'Category 6' },
      { position: 7, name: 'Product 7', price: 1467, category: 'Category 7' },
      { position: 8, name: 'Product 8', price: 9994, category: 'Category 8' },
      { position: 9, name: 'Product 9', price: 9984, category: 'Category 9' },
      { position: 10, name: 'Product 10', price: 1797, category: 'Category 10' },
    ];
  }

}
