import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { FileUploadService } from '../file-upload.service';
import { PublishItemPageService } from '../publish-item-page.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  panelOpenState = false;

  descriptionFormGroup = this._formBuilder.group({
    nameCtrl: ['', Validators.required],
    shortNameCtrl: ['', Validators.required],
    descriptionCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });
  thirdFormGroup = this._formBuilder.group({
    thirdCtrl: ['', Validators.required],
  });
  imagesFormGroup = this._formBuilder.group({
    imageNameCtrl: ['', Validators.required],
    imageIdCtrl: ['', Validators.required],
    bigimageNameCtrl: ['', Validators.required],
    bigimageIdCtrl: ['', Validators.required],
  });
  isLinear = false;


  constructor(
    private _formBuilder: FormBuilder,
    private publishItemPageService: PublishItemPageService,
    fileUploadService: FileUploadService,
    private _http: HttpClient
  ) { }

  data: any; // map this var to wen form fields

  ngOnInit(): void {
    this.publishItemPageService.getPageItem().subscribe((response) => {
      this.data = response;
      console.log("data: " + response);
    });
  }



  @ViewChild('fileUpload')
  fileUpload!: ElementRef;

  onClick(event: any) {
    if (this.fileUpload)
      this.fileUpload.nativeElement.click()

  }

}
