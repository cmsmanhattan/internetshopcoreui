import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'description', 'subCategories', 'subCategory', 'action'];
  dataSource: any;

  constructor() { }

  ngOnInit(): void {
    this.dataSource = [
      { position: 1, name: 'Category 1', price: 600, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 2, name: 'Category 2', price: 500, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }] },
      { position: 3, name: 'Category 3', price: 400, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 2" }] },
      { position: 4, name: 'Category 4', price: 922, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 5, name: 'Category 5', price: 811, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 6, name: 'Category 6', price: 2107, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 7, name: 'Category 7', price: 1467, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 8, name: 'Category 8', price: 9994, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 9, name: 'Category 9', price: 9984, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
      { position: 10, name: 'Category 10', price: 1797, description: 'Description', subCategories: [{ id: 1, name: "SubCategory 1" }, { id: 1, name: "SubCategory 2" }] },
    ];
  }
}
