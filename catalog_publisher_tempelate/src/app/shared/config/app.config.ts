
export interface AppConfig {
    apiUrl: any;
    projectName: string,
    developedBy: string
}
export const appConfig: AppConfig = {
    apiUrl: 'http://localhost:8080/api/v1/',
    projectName: 'CMS',
    developedBy: 'P4logics Pvt. Ltd.'
};
