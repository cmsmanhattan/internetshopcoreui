import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NavigationService {
    sessionUser: any;
    menuItems = new BehaviorSubject<any[]>([]);
    navbar: any[] = [];

    constructor(
    ) {
        this.setNavigation();
    }

    setNavigation() {
        this.navbar = [];
        this.navbar.push({
            title: 'Dashboard',
            icon: 'dashboard',
            link: '/admin/dashboard',
            type: 'link'
        });

        this.navbar.push({
            title: 'Product',
            icon: 'dashboard',
            link: '/admin/product',
            type: 'link'
        });

        this.navbar.push({
            title: 'Catelog',
            icon: 'dashboard',
            link: '/admin/category',
            type: 'link'
        });

        this.navbar.push({
            title: 'Publisher',
            icon: 'dashboard',
            link: '/admin/product/add',
            type: 'link'
        });

        this.menuItems.next(this.navbar);
    }
}
